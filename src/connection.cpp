#include "connection.hpp"

void CConnection::run() {
    auto self(shared_from_this());
    
    // Read URL
    m_socket.async_read_some(boost::asio::buffer(m_buffer),
        [this, self](boost::system::error_code error, std::size_t length) {
            if (error) {
              return;
            }

            std::string url(m_buffer.data(), length);

            // Remove CR and LF if they were added by telnet
            size_t pos = 0;
            while ((pos = url.find ("\r\n",pos)) != std::string::npos) {
              url.erase (pos, 2);
            }

            m_pipe = boost::process::async_pipe(m_context);

            // Initiate child curl-process. Add -L option for location(error 301) redirection
            m_child = boost::process::child(boost::process::args({"curl", "-L", url.c_str()}),
            // Redirect std out to pipe
            boost::process::std_out > m_pipe,
            // Redirect std_error to null
            boost::process::std_err > boost::process::null);
            // Initiate the first pipe read iteration
            self->onRead();
        });    
}

void CConnection::onRead() {
    auto self(shared_from_this());
    m_pipe.async_read_some(boost::asio::buffer(m_buffer),
        [this, self](boost::system::error_code error, std::size_t length) {
            if (length == 0) {
              return; 
            }
            if (length < m_buffer.size()) {
              m_buffer.resize(length);
            }
            self->onWrite();
        });
}

void CConnection::onWrite() {
    auto self(shared_from_this());
    m_socket.async_write_some(boost::asio::buffer(m_buffer),
        [this, self](boost::system::error_code error, std::size_t length)
        {
          if (m_buffer.size() != max_buffsize) {
            m_buffer.resize(max_buffsize);
          }
          self->onRead();
        });
}

CConnection::~CConnection() {
    if (m_child.joinable()) {
      m_child.join();
    }
}