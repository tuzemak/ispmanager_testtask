#ifndef SERVER_HPP
#define SERVER_HPP

#include <boost/asio.hpp>
#include "connection.hpp"

class CServer 
{
public:
    explicit CServer(boost::asio::io_context &context, unsigned int port);
    void onAccept();
private:
    boost::asio::io_context &m_context;
    boost::asio::ip::tcp::acceptor m_acceptor;

};

#endif // #ifndef SERVER_HPP