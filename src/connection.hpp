#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include <boost/asio.hpp>
#include <boost/process.hpp>
#include <vector>

class CConnection : public std::enable_shared_from_this<CConnection> {
public:
    explicit CConnection(boost::asio::io_context& context, boost::asio::ip::tcp::socket socket) 
    : m_buffer(max_buffsize)
    , m_context(context)
    , m_socket(std::move(socket))
    , m_pipe(m_context) {};
    
    virtual ~CConnection();

    void run();
    void onRead();
    void onWrite();
private:
    static const size_t max_buffsize = 1024;
    std::vector<char> m_buffer;
    boost::asio::io_context& m_context;
    boost::process::child m_child;
    boost::process::async_pipe m_pipe;
    boost::asio::ip::tcp::socket m_socket;
};

#endif // #ifndef CONNECTION_HPP