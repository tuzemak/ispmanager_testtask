#include "server.hpp"

CServer::CServer(boost::asio::io_context &context, unsigned int port)
    : m_context(context)
    , m_acceptor(m_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)) {
        onAccept();
}

void CServer::onAccept() {
    m_acceptor.async_accept([this](boost::system::error_code error, boost::asio::ip::tcp::socket socket)
        {
          if (!error)
          {
            std::make_shared<CConnection>(m_context, std::move(socket))->run();
          }
          onAccept();
        });
}