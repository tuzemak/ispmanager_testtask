
#include <boost/asio.hpp>
#include <iostream>
#include "server.hpp"


int main(int argc, char **argv) {
    if (argc == 1) {
        std::cout << "usage: async_server [PORT]" << std::endl;
        return 0;
    }

    if (argc == 2) {
        boost::asio::io_context context;

        boost::asio::signal_set signals(context, SIGINT, SIGTERM);
        signals.async_wait([&](const boost::system::error_code& error, int signal) { 
            context.stop(); 
        });

        CServer server(context, std::atoi(argv[1]));
        context.run();
        return 0;
    }

    std::cout << "Wrong options. Usage: async_server [PORT]" << std::endl;

    return 0;
}