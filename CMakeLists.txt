cmake_minimum_required(VERSION 3.1)

project(ispmanager_testtask)

find_package(Boost 1.38.0)

set(CMAKE_CXX_STANDARD 14)

set(SOURCES
    src/main.cpp
    src/connection.cpp
    src/server.hpp
)
set(HEADERS
    src/connection.hpp
    src/server.cpp
)

add_executable(async_server ${SOURCES} ${HEADERS})
